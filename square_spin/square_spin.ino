#include "LedControl.h"

LedControl lc=LedControl(12,11,10,1);

//sleeptime
unsigned long delaytime = 500;
//square size
const int sqr_size = 8;

void setup() {
  lc.shutdown(0,false);
  lc.setIntensity(0,8);
  lc.clearDisplay(0);
}


void square_spin() {

  for (int a = 0, b = sqr_size, x = 0, y = 0, position = 0; b > 0; a++, position++) {
    
    // First time 
    // bacause of sequence
    if (b == sqr_size) {
      b -= 1;
      a -= 1;
      lc.setLed(0,x,y,true);
      delay(delaytime);
      position = -1;
      continue;
    }

    // main spining part
    for (int n = 0; n < b; n++) {
      if (position == 0) x++;
      else if (position == 1) y++;
      else if (position == 2) x--;
      else if (position == 3) y--;
      lc.setLed(0,x,y,true);
      delay(delaytime);
      //cout << x << y << endl;
    }
    
    // this!=first time -------------------------
    if (a == 2) {
      b -= 1;
      a = 0;
      //cout << endl;
    }
    // restart spinnig algorithm
    if (position == 3) position = -1;
  }
}

void loop() { 
  square_spin();
}
