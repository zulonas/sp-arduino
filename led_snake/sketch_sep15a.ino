#include "LedControl.h"

LedControl lc=LedControl(12,11,10,1); // 12 -DIN, 11 - CLK, 10 - CS
#define button A0

int bordsize = 8;
unsigned long previousMillis = 0;  
const long interval = 500;  //snake speed 
int dir=1;
int ys[64];
int xs[64];
int snake_size = 3;

int applepos[2]= {random(bordsize), random(bordsize)};

void setup() {
  lc.shutdown(0,false);
  lc.setIntensity(0,9);
  lc.clearDisplay(0);
  Serial.begin(9600);
  //fist parts
  ys[0]=3; xs[0]=3;
  ys[1]=4; xs[1]=3;
  ys[2]=5; xs[2]=3;
}

void loop() {
  int sensorValue = analogRead(button);
  Serial.println(sensorValue);
  //get dir
  if(sensorValue<1027 && sensorValue>=1020 && dir!=2 ) dir = 1; //up
  else if(sensorValue<=1017 && sensorValue>=1007 && dir!=1) dir = 2; //down
  else if(sensorValue<=1000 && sensorValue>=990 && dir!=4) dir = 3; //left
  else if(sensorValue<=982 && sensorValue>=970 && dir!=3) dir = 4; //right

  if (millis() - previousMillis >= interval) {
    previousMillis = millis();
    
    lc.clearDisplay(0);

    int i = snake_size-1;
    while(i>= 2){
      if(collide(ys[0], ys[i], xs[0], xs[i]))
        sreset();
      i--;
    }

    if(collide(ys[0], applepos[0], xs[0], applepos[1])){
        snake_size++;
        applepos[0]=random(bordsize-1);
        applepos[1]=random(bordsize-1);
    }

    if(xs[0] < 0 || xs[0] >= bordsize || ys[0] < 0 || ys[0] >=bordsize)
         sreset();
    
    i=snake_size-1;
    while(i> 0){ // change all values in arr with perv !except first
      xs[i] = xs[i-1];
      ys[i] = ys[i-1];
      i--;
    }
    
    if (dir==1) ys[0]--;
    else if (dir==2) ys[0]++;
    else if (dir==3) xs[0]--;
    else if (dir==4) xs[0]++; 

    lc.setLed(0, applepos[0], applepos[1], true);
    
    for(int x=0; x<snake_size; x++){
      lc.setLed(0, ys[x], xs[x], true);
    } 
  }
}

bool collide(int y1, int y2, int x1, int x2){
    if(y1==y2 && x1==x2)
      return 1;
    else
      return 0;
}

//restarts program from beginning but does not reset the peripherals and registers
void sreset(){
  asm volatile ("  jmp 0");  
} 