#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN            6
#define NUMPIXELS      60

float left, right;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


void setup() {
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif


  Serial.begin(9600);
  pixels.begin();
  pixels.show();// This initializes the NeoPixel library.
}

void loop() {
  pixels.show();
  left=0;
  right=0;

  for(int x=0; x<100; x++){
    left += analogRead(A0);
    right += analogRead(A1);
  }
  
  left /= 100; 
  right /= 100;

  if(left>200||right>200){
    rainbowCycle(20);
  }
  else{
    music_lside(right);
    music_rside(left);
  }
 
  delay(0);
}


void music_lside(float nubr){
  float hop;
  hop = (30*nubr)/23;
  hop = floor(hop);

  for(int x=0; x<30; x++){
    if(x<hop)
      pixels.setPixelColor(x, pixels.Color(255,0,0));
    else
      pixels.setPixelColor(x, pixels.Color(0,0,0));
  }

}

void music_rside(float nubr){
  float hop;
  hop = (30*nubr)/23;
  hop = round(hop);
  hop = 60-hop;

  for(int x=59; x>29; x--){
    if(x>hop)
      pixels.setPixelColor(x, pixels.Color(0,255,0));
    else
      pixels.setPixelColor(x, pixels.Color(0,0,0));
  }

}

void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< pixels.numPixels(); i++) {
      pixels.setPixelColor(i, Wheel(((i * 256 / pixels.numPixels()) + j) & 255));
    }
    pixels.show();
    delay(wait);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

